import Vue from 'vue'
import Router from 'vue-router'
import DashboardLayout from '@/layout/DashboardLayout'
import AuthLayout from '@/layout/AuthLayout'
import TreeView from "vue-json-tree-view"
Vue.use(Router)
Vue.use(TreeView)
export default new Router({
  linkExactActiveClass: 'active',
  routes: [
    {
      path: '/',
      redirect: 'login',
      component: DashboardLayout,
      children: [
        {
          path: '/dashboard/:id',
          name: 'dashboard',
          // route level code-splitting
          // this generates a separate chunk (about.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () => import(/* webpackChunkName: "demo" */ './views/Dashboard.vue')
        },
        {
          path: '/icons',
          name: 'icons',
          component: () => import(/* webpackChunkName: "demo" */ './views/Icons.vue')
        },
        {
          path: '/profile',
          name: 'profile',
          component: () => import(/* webpackChunkName: "demo" */ './views/UserProfile.vue')
        },
        {
          path: '/tables',
          name: 'tables',
          component: () => import(/* webpackChunkName: "demo" */ './views/Tables.vue')
        },
        {
          path: '/newstartup',
          name: 'newstartup',
          component: () => import(/* webpackChunkName: "demo" */ './views/newstartup.vue')
        },
        {
          path: '/members/:id/startupid',
          name: 'members',
          component: () => import(/* webpackChunkName: "demo" */ './views/members.vue')
        },
        {
          path: '/Investors',
          name: 'Investors',
          component: () => import(/* webpackChunkName: "demo" */ './views/Investors.vue')
        },
        {
          path: '/New_investor',
          name: 'New_investor',
          component: () => import(/* webpackChunkName: "demo" */ './views/New_investor.vue')
        },
        {
          path: '/New_member/:id',
          name: 'New_member',
          component: () => import(/* webpackChunkName: "demo" */ './views/New_member.vue')
        },
        {
          path: '/capitalization',
          name: 'capitalization',
          component: () => import(/* webpackChunkName: "demo" */ './views/capitalization table.vue')
        },
        {
          path: '/new_capitalization',
          name: 'new_capitalization',
          component: () => import(/* webpackChunkName: "demo" */ './views/new_capitalization.vue')
        },
        {
  path: '/New_file/:id',
  name: 'New_file',
  component: () => import(/* webpackChunkName: "demo" */ './views/New_file.vue')
},
{
  path: '/show_file/:id/:name',
  name: 'show_file',
  component: () => import(/* webpackChunkName: "demo" */ './views/show_file.vue')
},
{
  path: '/Edit_file/:id/:name',
  name: 'Edit_file',
  component: () => import(/* webpackChunkName: "demo" */ './views/Edit_file.vue')
},
{
  path: '/users',
  name: 'users',
  component: () => import(/* webpackChunkName: "demo" */ './views/users.vue')
},

        {
               path: '/Editstartup/:id',
               name: 'Editstartupt',
             component:() => import(/* webpackChunkName: "demo" */ './views/Editstartup.vue')
 },
 {
        path: '/Edituser/:id',
        name: 'Edituser',
      component:() => import(/* webpackChunkName: "demo" */ './views/Edituser.vue')
},
 {
        path: '/Edit data/:id',
        name: 'Edit data',
      component:() => import(/* webpackChunkName: "demo" */ './views/Edit data.vue')
},
{
       path: '/Edit issue/:id/:issueid',
       name: 'Edit issue',
     component:() => import(/* webpackChunkName: "demo" */ './views/Edit issue.vue')
},
{
       path: '/New_issue_file/:id/:issueid',
       name: 'New_issue_file',
     component:() => import(/* webpackChunkName: "demo" */ './views/New_issue_file.vue')
},
 {
        path: '/Editinvestor/:id',
        name: 'Editinvestor',
      component:() => import(/* webpackChunkName: "demo" */ './views/Editinvestor.vue')
},
{
       path: '/Editmember/:id',
       name: 'Editmember',
     component:() => import('./views/Editmember.vue')
},
{
       path: '/Edit KPI/:id/:kpi_id',
       name: 'Edit KPI',
     component:() => import('./views/Edit KPI.vue')
},
{
       path: '/Editcategory/:id',
       name: 'Editcategory',
     component:() => import(/* webpackChunkName: "demo" */ './views/Editcategory.vue')
},
{
       path: 'New_Groupaccount',
       name: 'New_Groupaccount',
     component:() => import(/* webpackChunkName: "demo" */ './views/New_Groupaccount.vue')
},
{
       path: 'New_Event/:name',
       name: 'New_Event',
     component:() => import(/* webpackChunkName: "demo" */ './views/New_Event.vue')
},
{
       path: 'New_account/id',
       name: 'New_account',
     component:() => import(/* webpackChunkName: "demo" */ './views/New_account.vue')
},
{
       path: 'New_user',
       name: 'New_user',
     component:() => import(/* webpackChunkName: "demo" */ './views/New_user.vue')
},
{
       path: 'New_transaction',
       name: 'New_transaction',
     component:() => import(/* webpackChunkName: "demo" */ './views/New_transaction.vue')
},
{
       path: '/Editshares/:id',
       name: 'Editshares',
     component:() => import(/* webpackChunkName: "demo" */ './views/Editshares.vue')
},
{
       path: '/New_category',
       name: 'New_category',
     component:() => import(/* webpackChunkName: "demo" */ './views/New_category.vue')
},
{
       path: '/New_frequency',
       name: 'New_frequency',
     component:() => import(/* webpackChunkName: "demo" */ './views/New_frequency.vue')
},
{
       path: '/New_KPI',
       name: 'New_KPI',
     component:() => import(/* webpackChunkName: "demo" */ './views/New_KPI.vue')
},
 {
   path: '/kpis/:id',
   name: 'kpis',
   component: () => import(/* webpackChunkName: "demo" */ './views/kpis.vue')
 },
 {
   path: '/categories',
   name: 'categories',
   component: () => import(/* webpackChunkName: "demo" */ './views/categories.vue')
 } ,
 {
    path: '/kpi data',
    name: 'kpi data',
    component: () => import(/* webpackChunkName: "demo" */ './views/kpi data.vue')
  },
  {
     path: '/groups',
     name: 'groups',
     component: () => import(/* webpackChunkName: "demo" */ './views/groups.vue')
   },
   {
      path: '/accounts',
      name: 'accounts',
      component: () => import(/* webpackChunkName: "demo" */ './views/accounts.vue')
    },
    {
           path: '/transactions/:id',
           name: 'transactions',
         component:() => import(/* webpackChunkName: "demo" */ './views/transactions.vue')
   },
  {
    path: '/New_data',
    name: 'New_data',
    component: () => import(/* webpackChunkName: "demo" */ './views/New_data.vue')
  },
  {
    path: '/projects',
    name: 'projects',
    component: () => import(/* webpackChunkName: "demo" */ './views/projects.vue')
  },
  {
    path: '/New_issue/:id',
    name: 'New_issue',
    component: () => import(/* webpackChunkName: "demo" */ './views/New_issue.vue')
  },
  {
    path: '/Issues/:id',
    name: 'Issues',
    component: () => import(/* webpackChunkName: "demo" */ './views/Issues.vue')
  },
  {
  path: '/projects_content',
  name: 'projects_content',
  component: () => import(/* webpackChunkName: "demo" */ './views/projects_content.vue')
},
{
  path: '/files/:id/:name',
  name: 'files',
  component: () => import(/* webpackChunkName: "demo" */ './views/files.vue')
},
{
path: '/calendar',
name: 'calendar',
component: () => import(/* webpackChunkName: "demo" */ './views/calendar.vue')
},
      ]
    },

    {
      path: '/',
      redirect: 'login',
      component: AuthLayout,
      children: [
        {
          path: '/login',
          name: 'login',
          component: () => import(/* webpackChunkName: "demo" */ './views/Login.vue')
        },
        {
          path: '/register',
          name: 'register',
          component: () => import(/* webpackChunkName: "demo" */ './views/newstartup.vue')
        }
      ]
    }
  ]
})

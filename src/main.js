/*!

=========================================================
* Vue Argon Dashboard - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'
import VueRouter from 'vue-router';
import ArgonDashboard from './plugins/argon-dashboard'
import Notifications from 'vue-notification'
import FullCalendar from 'vue-full-calendar'
Vue.use(FullCalendar)
//import  store  from  './store'
import  Axios  from  'axios'

import { VuejsDatatableFactory } from 'vuejs-datatable';
import 'vue-event-calendar/dist/style.css' //^1.1.10, CSS has been extracted as one file, so you can easily update it.
import vueEventCalendar from 'vue-event-calendar'
Vue.use(vueEventCalendar, {locale: 'en'})

Vue.config.productionTip  =  false
Vue.prototype.$http  =  Axios;
const  accessToken  =  localStorage.getItem('access_token')

if (accessToken) {
    Vue.prototype.$http.defaults.headers.common['Authorization'] =  accessToken
}
Vue.use(ArgonDashboard)
Vue.use(VueRouter)
Vue.use(Notifications)
Vue.use( VuejsDatatableFactory );

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
